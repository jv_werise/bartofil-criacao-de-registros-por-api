/*******************************************************************************
*                               Werise - 2021
*-------------------------------------------------------------------------------
*
*    Class to create mock callout request and return the expected body
*
* USED IN: GetSintegraRequest_test
* NAME: HttpCalloutMockSintegra
*
* AUTHOR: João Vitor Ramos                                DATE: 12/04/2021
*******************************************************************************/
@isTest
global class HttpCalloutMockSintegra implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"code":"0","status":"OK","message":"Pesquisa realizada com sucesso.","cnpj":"07819090000196","inscricao_estadual":"442207308118","nome_empresarial":"Copacitrus industria e comercio de frutas ltda","nome_fantasia":"","situacao_cnpj":"Sem restrição","situacao_ie":"Ativo","cnae_principal":{"code":"1033301","text":"Fabricação de sucos concentrados de frutas, hortaliças e legumes"},"cep":"09372080","uf":"SP","municipio":"Mauá","bairro":"Loteamento industrial coral","logradouro":"Rua das camelias","numero":"427","complemento":null,"regime_tributacao":"Simples nacional","informacao_ie_como_destinatario":"Não informado","porte_empresa":"Não informado","tipo_inscricao":"","data_inicio_atividade":"03-03-2006","data_fim_atividade":"","data_situacao_cadastral":"03-03-2006","ibge":{"codigo_municipio":"3529401","codigo_uf":"35"},"ccc":false,"version":"5"}');
        response.setStatusCode(200);
        return response; 
    }
}