/*******************************************************************************
*                               Werise - 2021
*-------------------------------------------------------------------------------
*
*    Class to test the GetSintegraRequest process
*
* NAME: GetSimplesNacionalRequest_test
*
* AUTHOR: Fabrício Garcia                                DATE: 23/03/2021
*******************************************************************************/
@isTest
public class GetSimplesNacionalRequest_test {
    
    

    @isTest
    public static void testOk() {
        
        Test.startTest();    
        System.enqueueJob(new GetSimplesNacionalRequest(''));
        System.enqueueJob(new GetSimplesNacionalRequest('ab2')); //para gerar o catch(Exception e)
        Test.stopTest();
    }
  
}