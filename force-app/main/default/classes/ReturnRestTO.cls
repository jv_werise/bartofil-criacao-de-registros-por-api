/*******************************************************************************
 *                               Werise - 2020
 *-------------------------------------------------------------------------------
 *
 *    Interface object for service response
 *
 * NAME: ReturnRestTO
 * TESTCLASS: OrcamentoNaoAssociadoRest_test || LeadRest_test
 *
 * AUTHOR: Jefferson F. Presotto                                DATE: 14/07/2020
 *******************************************************************************/
public class ReturnRestTO{
	public List<Object> data{ get; set; }

	public List<Object> errors{ get; set; }

	public MetaTO meta{ get; set; }

	public ReturnRestTO(String serviceName){
		this.meta = new MetaTO();
		this.meta.service = serviceName;
		this.meta.dateExecution = System.now();
	}

	public class MetaTO{
		public String service{ get; set; }

		public Datetime dateExecution{ get; set; }

		public Integer statusHttp{ get; set; }

		public String message{ get; set; }
	}

	public class Errors{
		private String erro;
		private Object data;
		public Errors(String erro, Object data){
			this.data = data;
			this.erro = erro;
		}
	}
}
