/*******************************************************************************
*                               Werise - 2021
*-------------------------------------------------------------------------------
*
*    Class to create mock callout request and return the expected body
*
* USED IN: GetReceitaRequest_test
* NAME: HttpCalloutMockReceita
*
* AUTHOR: João Vitor Ramos                                DATE: 12/04/2021
*******************************************************************************/
@isTest
global class HttpCalloutMockReceita implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"atividade_principal":[{"text":"Fabricação de sucos concentrados de frutas, hortaliças e legumes","code":"10.33-3-01"}],"data_situacao":"10/10/2005","tipo":"MATRIZ","nome":"COPACITRUS INDUSTRIA E COMERCIO DE FRUTAS LTDA","uf":"SP","telefone":"(11) 4220-2277","email":"copacitrus@uol.com.br","atividades_secundarias":[{"text":"Fabricação de sucos de frutas, hortaliças e legumes, exceto concentrados","code":"10.33-3-02"}],"qsa":[{"qual":"49-Sócio-Administrador","nome":"GIULIANA AIDA ORSATTI"},{"qual":"49-Sócio-Administrador","nome":"MARIANA AIDA ORSATTI"}],"situacao":"ATIVA","bairro":"SERTAOZINHO","logradouro":"R DAS CAMELIAS","numero":"427","cep":"09.370-843","municipio":"MAUA","porte":"MICRO EMPRESA","abertura":"10/10/2005","natureza_juridica":"206-2 - Sociedade Empresária Limitada","cnpj":"07.819.090/0001-96","ultima_atualizacao":"2021-04-12T13:37:53.385Z","status":"OK","fantasia":"","complemento":"","efr":"","motivo_situacao":"","situacao_especial":"","data_situacao_especial":"","capital_social":"15000.00","extra":{},"billing":{"free":false,"database":true}}');
        response.setStatusCode(200);
        return response; 
    }
}
