/*******************************************************************************
 *                               Werise - 2021
 *-------------------------------------------------------------------------------
 *
 *    Class to build the send data structure
 *
 * NAME: PostObjectTO
 * TESTCLASS: PostLeadData_test
 *
 * AUTHOR: Jefferson F. Presotto                                DATE: 23/03/2021
 *******************************************************************************/
public class PostObjectTO{
	private List<Object> data;
    private Metadata meta = new Metadata();
    
	public PostObjectTO(String service, List<Object> objects){
		this.meta.service = service;
		this.meta.dateExecution = System.now();
		this.data = objects;
	}
    public class Metadata{
		public String service{ get; set; }
		public Datetime dateExecution{ get; set; }
	}
}