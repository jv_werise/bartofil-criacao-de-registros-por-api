/*******************************************************************************
*                               Werise - 2021
*-------------------------------------------------------------------------------
*
*    GetSintegraRequest
*
* NAME: GetSintegraRequest
* TEST CLASS: GetSintegraRequest_test
*
* AUTHOR: João Vitor Ramos                                DATE: 26/03/2021
*******************************************************************************/
public class GetSintegraRequest implements Queueable, Database.AllowsCallouts{
    //Received account list to use
	private List<Account> accList = new list<Account>();
    
	public GetSintegraRequest(List<Account> records){
		this.accList = records;
	}
    public void execute(QueueableContext context){
        //lists and maps to handle the logic
		List<Account> accUpdList = new List<Account>();
		Map<String, Object> accountMap = new Map<String, Object>();

        List<Account> accFilteredList = [SELECT Id, Name, Cnpj__c, RecordType.DeveloperName, inscricaorg__c FROM Account WHERE Id IN :accList];

		//Pass through each account retrieving the informations from government's API
		for(Account acc : accFilteredList){
            System.debug(acc.cnpj__c);
            System.debug(acc.inscricaorg__c);
            System.debug(acc.RecordType.DeveloperName);
			if(acc.cnpj__c == null || acc.inscricaorg__c == null || acc.RecordType.DeveloperName != 'Cliente_Pessoa_Juridica'){
				accUpdList.add(acc);
			}else{
                try{
                    //Make the http get request
                    Http http = new Http();
                    HttpRequest request = new HttpRequest();
                    request.setEndpoint('https://bartofil.hubin.io/iomanager/api/flows/execute/route/sintegra/'+acc.cnpj__c);
                    request.setMethod('GET');
                    request.setHeader('Content-Type', 'application/json');
                    request.setHeader('Charset', 'UTF-8');
                    request.setTimeout(120000);
                    HttpResponse response = http.send(request);

                    //put the retrieved information on account sObject and add to the list to update
                    if (response.getStatusCode() == 200){
                        accountMap = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
						if((String)accountMap.get('status') == 'ERROR'){
							accUpdList.add(acc);
						}
						else{
							acc.Atividade_1_Sintegra__c = (String)(((Map<String, Object>)accountMap.get('cnae_principal')).get('text'));
                            acc.Situacao_IE_Sintegra__c = (String)(accountMap.get('situacao_ie'));
							acc.Situacao_CNPJ_Sintegra__c = (String)(accountMap.get('situacao_cnpj'));
                            acc.dtasintegra__c = Date.today();
							acc.data_situacao_sintegra__c = Date.newInstance(Integer.valueOf((((String)accountMap.get('data_situacao_cadastral')).split('-'))[2]), Integer.valueOf((((String)accountMap.get('data_situacao_cadastral')).split('-'))[1]), Integer.valueOf((((String)accountMap.get('data_situacao_cadastral')).split('-'))[0]));
                            acc.data_inicio_sintegra__c = Date.newInstance(Integer.valueOf((((String)accountMap.get('data_inicio_atividade')).split('-'))[2]), Integer.valueOf((((String)accountMap.get('data_inicio_atividade')).split('-'))[1]), Integer.valueOf((((String)accountMap.get('data_inicio_atividade')).split('-'))[0]));

							acc.enderrua__c = (String)(accountMap.get('logradouro'));
							acc.enderbairro__c = (String)(accountMap.get('bairro'));
							acc.endercidade__c = (String)(accountMap.get('municipio'));
							acc.enderuf__c = (String)(accountMap.get('uf'));
							acc.endercep__c = (String)(accountMap.get('cep'));
                            acc.endercomplemento__c = (String)(accountMap.get('complemento'));
							if((accountMap.get('numero')) == null || (accountMap.get('numero')) == '' || (accountMap.get('numero')) == ' '){
								acc.endernumero__c =  's/n';
							} else {
								acc.endernumero__c = (String)(accountMap.get('numero'));
							}
							//Adding to the update list
							accUpdList.add(acc);
                            System.debug(acc);
						}
                    } else{
                        System.debug('Error response: ' + response.getStatusCode());
						System.debug('Error Body: ' + response.getBody());
                        accUpdList.add(acc);
                    }
                } catch (Exception e){
                    System.debug('ERROR ON REQUEST: ' + e.getMessage()+'. In line: ' + e.getLineNumber());
                    accUpdList.add(acc);
                }
            }
        }
        //updating the Accounts and validating the result
        Database.SaveResult[] results = Database.update(accUpdList, false);
        for (Database.SaveResult sr : results){
            if (!sr.isSuccess()){
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred. '+err.getStatusCode() + ': ' + err.getMessage()+' Contact fields that affected this error: ' + err.getFields());
                }
            }
        }
        //chamar proxima ou enviar contas para bartofil/wevo
    }
}