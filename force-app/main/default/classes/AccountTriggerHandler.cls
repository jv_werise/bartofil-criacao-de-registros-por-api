/*******************************************************************************
 *                               Werise - 2021
 *-------------------------------------------------------------------------------
 *
 *    Class to handle logic on dml events of Bartofil system.
 *
 * NAME: AccountTriggerHandler
 * TESTCLASS: AccountTriggerHandler_test
 *
 * AUTHOR: João Vitor Ramos                                DATE: 23/03/2021
 *******************************************************************************/
public class AccountTriggerHandler {

    public static void atribuirContaEmRedeCpf(list<Account> records){
        //Declaring the lists, Maps and sets to handle the logic
        list<Account> accFilteredList = new list<Account>();
        set<String> cpfSet = new set<String>();
        list<Account> accsRedeList = new list<Account>();
        //list<Account> accUpdList = new list<Account>();
        Map<Id, Account> accUpdMap = new Map<Id, Account>();

        //filtering accounts by RecordType
        for(Account acc : [SELECT id, CPF__c FROM Account WHERE RecordType.DeveloperName = 'Cliente_Pessoa_Fisica' 
                                                            AND id IN :records ORDER BY CPF__c,CreatedDate]){
                accFilteredList.add(acc);
        }

        if(accFilteredList.isEmpty()){
            System.debug('Lista de Cliente_Pessoa_Fisica vazia');
            return;
        } 

        for(Account acc : accFilteredList){
            cpfSet.add(acc.CPF__c);
        }

        //Retrieving account with the same CPF of the triggered records
        accsRedeList = [SELECT id, lastname, CPF__c, CreatedDate, limitecredito__c  FROM Account WHERE CPF__c IN :cpfSet ORDER BY cpf__c, CreatedDate DESC];
        System.debug(accsRedeList.size());

        for(String cpf : cpfSet){
            list<Account> cpfFilteredList = new list<Account>();
            for(Account acc : accsRedeList){
                if(cpf == acc.cpf__c){
                    cpfFilteredList.add(acc);
                }
            }

            //Validating whether the CPF is equal and the Conta_em_Rede__c field is null to fill the with the right Id
            //Take care when loanding the data from legacy system to salesforce platform at once
            //because the CreatedDate, when inserting a list, is the same to all records in a dml. So we can't differ
            //the old(parent) recods with the new if they were loaded at same time.
            Integer index = 1;
            for(Account acc : cpfFilteredList){
                if(index == cpfFilteredList.size()){
                    accUpdMap.put(acc.Id, acc);
                    break;
                }
                for(Account acc2 : cpfFilteredList){
                    if(acc.CPF__c == acc2.CPF__c && acc.Id != acc2.Id && acc.CreatedDate >= acc2.CreatedDate){
                        acc.Conta_em_Rede__c = acc2.Id;
                        accUpdMap.put(acc.Id, acc);
                    }
                }
                index++;
            }

            //Summing the limitecredito__c to get the total amount and fiiling the Limite_Compartilhado__c to all cpf related Accounts
            Decimal limiteCompartilhado = 0;
            for(Account acc : accUpdMap.values()){
                if(acc.limitecredito__c != null){
                    limiteCompartilhado += acc.limitecredito__c;
                }
            }
            for(Account acc : accUpdMap.values()){
                acc.Limite_Compartilhado__c = limiteCompartilhado;
                accUpdMap.put(acc.Id, acc);
            }
        }

        if(!accUpdMap.isEmpty()){
            //updating filial Accounts and validating the result
            Database.SaveResult[] results = Database.update(accUpdMap.values(), false);
            for (Database.SaveResult sr : results){
                if (!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred in atribuirContaEmRedeCpf. '+err.getStatusCode() + ': ' + err.getMessage()+' Account fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
    }

    public static void atribuirContaEmRedeCnpj(list<Account> records){
        //Declaring the lists, maps and sets to handle the logic
        List<Account> accFilteredList = new List<Account>();
        Map<Id, Account> accUpdMap = new Map<Id, Account>();
        List<Account> matrizList = new List<Account>();
        List<Account> matrizRelatedList = new List<Account>();
        List<Account> filialList = new List<Account>();
        List<Account> filialRelatedList = new List<Account>();
        set<String> cnpjMatrizSet = new set<String>();
        set<String> cnpjFilialSet = new set<String>();
        Map<String, Id> raizMatrizIdMap = new Map<String, Id>();
        Set<String> allRaizCnpjSet = new Set<String>();
        List<Account> allAccRelated = new List<Account>();

        //filtering accounts by RecordType
        for(Account acc : [SELECT Id, Name, RecordType.DeveloperName, Cnpj__c, raiz_de_cnpj__c, Verifica_Matriz__c FROM Account 
                           WHERE Id IN :records AND RecordType.DeveloperName = 'Cliente_Pessoa_Juridica']){
            accFilteredList.add(acc);
        }

        if(accFilteredList.isEmpty()){
            System.debug('Lista de Cliente_Pessoa_Juridica vazia');
            return;
        } 

        //Filtering the Accounts with either Matriz(HeadQuarter) and Filial(Branch) status to separate them in the logic
        matrizList = [SELECT Id, Cnpj__c, raiz_de_cnpj__c FROM Account WHERE Verifica_Matriz__c = true AND Id IN :accFilteredList];
        filialList = [SELECT Id, Cnpj__c, raiz_de_cnpj__c FROM Account WHERE Verifica_Matriz__c = false AND Id IN :accFilteredList];


        if(!matrizList.isEmpty()){
            for(Account acc : matrizList){
                cnpjMatrizSet.add(acc.raiz_de_cnpj__c);
            }

            //Filtering the Filial(Branch) Accounts related to Matriz Accounts matching by Cnpj__c
            matrizRelatedList = [SELECT Id, Conta_em_Rede__c, Cnpj__c, raiz_de_cnpj__c FROM Account WHERE Verifica_Matriz__c = false AND raiz_de_cnpj__c IN :cnpjMatrizSet];

            //Passing through each Matriz and Filial Accounts matching the Cnpj and changing the Filial Accounts values
            if(!matrizRelatedList.isEmpty()){
                for(Account matriz : matrizList){
                    for(Account filial : matrizRelatedList){
                        if(filial.raiz_de_cnpj__c == matriz.raiz_de_cnpj__c){
                            filial.ParentId = matriz.Id;
                            accUpdMap.put(filial.Id, filial);
                        }
                    }
                }
            }
        }

        if(!filialList.isEmpty()){
            for(Account acc : filialList){
                cnpjFilialSet.add(acc.raiz_de_cnpj__c);
            }

            //Filtering the Matriz(HeadQuarter) Accounts related to branch Accounts matching by Cnpj__c
            filialRelatedList = [SELECT Id, Conta_em_Rede__c, Cnpj__c, raiz_de_cnpj__c FROM Account WHERE Verifica_Matriz__c = true AND raiz_de_cnpj__c IN :cnpjFilialSet ORDER BY CreatedDate desc];
            if (!filialRelatedList.isEmpty()) {
                for(Account acc : filialRelatedList){
                    raizMatrizIdMap.put(acc.raiz_de_cnpj__c, acc.Id);
                }
            }

            if(!raizMatrizIdMap.isEmpty()){
                for(Account filial : filialList){
                    for(String matriz : raizMatrizIdMap.keyset()){
                        if(matriz == filial.raiz_de_cnpj__c){
                            filial.ParentId = raizMatrizIdMap.get(matriz);
                            accUpdMap.put(filial.Id, filial);
                        }
                    }
                }
            }
        }

        if(!accUpdMap.isEmpty()){
            //updating Accounts and validating the result
            Database.SaveResult[] results = Database.update(accUpdMap.values(), false);
            for (Database.SaveResult sr : results){
                if (!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred in atribuirContaEmRedeCnpj. '+err.getStatusCode() + ': ' + err.getMessage()+' Account fields that affected this error: ' + err.getFields());
                    }
                }
            }
            accUpdMap.clear();
        }

        //Summing the limitecredito__c to get the total amount and fiiling the Limite_Compartilhado__c to all cpf related Accounts
        for(Account acc: accFilteredList){
            allRaizCnpjSet.add(acc.raiz_de_cnpj__c);
        }

        //Retrieving all Accounts related to cnpj's root of triggered records
        allAccRelated = [SELECT id, raiz_de_cnpj__c, limitecredito__c FROM Account 
                        WHERE raiz_de_cnpj__c IN :allRaizCnpjSet ORDER BY raiz_de_cnpj__c];

        //Passing through each cnpj's root, creating a filtered list and updating the Account with the summed value
        for(String cnpj : allRaizCnpjSet){
            list<Account> cnpjFilteredList = new list<Account>();
            for(Account acc : allAccRelated){
                if(cnpj == acc.raiz_de_cnpj__c){
                    cnpjFilteredList.add(acc);
                }
            }

            //Summing limiteCompartilhado
            Decimal limiteCompartilhado = 0;
            for(Account acc : cnpjFilteredList){
                if(acc.limitecredito__c != null){
                    limiteCompartilhado += acc.limitecredito__c;
                }
            }
            //Filling Limite_Compartilhado__c fields
            for(Account acc : cnpjFilteredList){
                acc.Limite_Compartilhado__c = limiteCompartilhado;
                accUpdMap.put(acc.Id, acc);
            }
        }

        if(!accUpdMap.isEmpty()){
            //updating Accounts and validating the result
            Database.SaveResult[] results = Database.update(accUpdMap.values(), false);
            for (Database.SaveResult sr : results){
                if (!sr.isSuccess()){
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred in atribuirContaEmRedeCnpj. '+err.getStatusCode() + ': ' + err.getMessage()+' Account fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
    }

    public static void enviarContasBartofil(list<Account> records){
        //Setting the json payload to send to queueable class
        PostObjectTO payload = new PostObjectTO('Contas', records);
		Id jobId = System.enqueueJob(new AccountTriggerHandler.QueueableAccountCall(JSON.serialize(payload)));
    }

    public class QueueableAccountCall implements Queueable, Database.AllowsCallouts{
        //Setting the request's payload
		private String payload;
		public QueueableAccountCall(String json){
			this.payload = json;
		}

       public void execute(QueueableContext context){
            //Make the http post request
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint('https://0b71210a-31d8-450b-be4a-3cd91a8f1faa.mock.pstmn.io/contas');
            request.setMethod('POST');
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Charset', 'UTF-8');
            request.setTimeout(120000);
            request.setBody(payload);
            try{
                HttpResponse response = http.send(request);

                if (response.getStatusCode() == 200){
                    Map<String, Object> accountMap = (Map<String, Object>)JSON.deserializeUntyped(RestContext.request.requestBody.toString());
                    List<Account> updAccountsList = (List<Account>)JSON.deserialize(JSON.serialize(accountMap.get('data')), List<Account>.class);

                    if(!updAccountsList.isEmpty()){
                        update updAccountsList;
                    }
                } else{
                    System.debug('Error Status code: ' + response.getStatusCode());
                    System.debug('Error Body: ' + response.getBody());
                }
            } catch (Exception e){
                System.debug('ERROR ON REQUEST: ' + e.getMessage()+'. In line: ' + e.getLineNumber());
            }
        }
    }
}