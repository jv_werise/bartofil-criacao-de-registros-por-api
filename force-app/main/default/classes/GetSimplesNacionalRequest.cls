/*******************************************************************************
 *                               Werise - 2021
 *-------------------------------------------------------------------------------
 *
 *    Class to get informations about CNPJ of Bartofil system.
 *
 * NAME: GetSimplesNacionalRequest
 * TEST CLASS: GetSimplesNacionalRequest_test
 *
 * AUTHOR: Fabrício Garcia                                DATE: 23/03/2021
 *******************************************************************************/

public class GetSimplesNacionalRequest implements Queueable, Database.AllowsCallouts{

	private String cnpj;
    
	public GetSimplesNacionalRequest(String cnpjString){
		this.cnpj = cnpjString;
	}

	public void execute(QueueableContext context){
		try{
            
			//Make the http get request
			Http http = new Http();
			HttpRequest request = new HttpRequest();
			request.setEndpoint('https://bartofil.hubin.io/iomanager/api/flows/execute/route/simples/'+cnpj);
			request.setMethod('GET');
			request.setHeader('Content-Type', 'application/json');
			request.setHeader('Charset', 'UTF-8');
			request.setTimeout(120000);
			HttpResponse response = http.send(request);
			if (response.getStatusCode() == 200){
				System.debug('response: ' + response.getStatusCode());
				System.debug('response: ' + response.getBody());
			} else{
				System.debug('response: ' + response.getStatusCode());
				System.debug('response: ' + response.getBody());
			}
		} catch (Exception e){
			System.debug('ERROR: ' + e.getMessage()+'. In line: ' + e.getLineNumber());
		}
	}
}