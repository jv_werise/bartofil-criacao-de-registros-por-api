/*******************************************************************************
 *                               Werise - 2021
 *-------------------------------------------------------------------------------
 *
 *    Class to receive the Post data of Bartofil system.
 *
 * NAME: AccountRest
 * TESTCLASS: AccountRest_test
 *
 * AUTHOR: João Vitor Ramos                                DATE: 04/02/2021
 *******************************************************************************/
@RestResource(urlMapping = 
'/Conta')
global class AccountRest extends ServiceRestBase{
	private static final AccountRest instance = new AccountRest();
	global AccountRest(){
		super.setServiceRestBase('accountService');
	}

	@HttpPut
	global static void processPut(){
		instance.processService('PUT');
	}

	global override void processService(String method){
		try{
            list<Account> successList = new list<Account>();
            List<ReturnRestTO.Errors> returnErros = new List<ReturnRestTO.Errors>();
            map<String,Id> map_RtName_RtId = new map<String,Id>();
            Map<String, Object> accResultMap = new Map<String, Object>();
            list<Account> accUpdList = new list<Account>();

			RestRequest req = RestContext.request;
			String endpoint = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);

            super.setServiceRestBaseName('Criar-Atualizar-Contas');
            Map<String, Object> accountMap = (Map<String, Object>)JSON.deserializeUntyped(RestContext.request.requestBody.toString());
            List<Account> newAccountsList = (List<Account>)JSON.deserialize(JSON.serialize(accountMap.get('contas')), List<Account>.class);
            System.debug(newAccountsList);

            //validate if has data
            if(newAccountsList == null || newAccountsList.isEmpty()){
                this.returnError('NO RECORDS', 400);
                return;
            }

            for(Account acc : newAccountsList){
                acc.Criado_Bartofil__c = true;
                accUpdList.add(acc);
            }


            /**upsert the Cases and validate the result*/
            Database.UpsertResult[] results = Database.upsert(accUpdList,Account.codpessoa__c, false);
            Integer index = 0;
            for (Database.UpsertResult ur : results){
                Account acc = accUpdList[index];
                if (ur.isSuccess()){
                    successList.add(acc);
                    index++;
                } else{
                    returnErros.add(new ReturnRestTO.Errors(ur.getErrors()[0].getMessage(), acc));
                    index++;
                }
            }
            this.returnSuccess(200, successList, returnErros);
		} catch (Exception e){
			if (e != null){
				this.returnError(e.getMessage()+' - ' + e.getLineNumber(), 400);
			} else{
				this.returnError(e.getMessage()+' - ' + e.getLineNumber());
			}
		}
	}
}
