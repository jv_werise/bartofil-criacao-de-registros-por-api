/*******************************************************************************
 *                               Werise - 2020
 *-------------------------------------------------------------------------------
 *
 *    Base Class for a service rest
 *
 *
 * NAME: ServiceRestBase
 * TESTCLASS: OrcamentoNaoAssociadoRest_test && LeadRest_test
 *
 * AUTHOR: Jefferson F. Presotto                                DATE: 14/07/2020
 *******************************************************************************/
global abstract without sharing class ServiceRestBase{
	private ReturnRestTO responseData{ get; set; }

	private String serviceName{ get; set; }

	private RestResponse response{ get; set; }

	private Boolean isReturnMetadata{ get; set; }

	//Method HTTP protocol method
	global abstract void processService(String method);
	//set name of service and instance response object
	global void setServiceRestBase(String nameService){
		this.responseData = new ReturnRestTO(nameService);
		this.response = RestContext.response;
		this.isReturnMetadata = true;
	}

	//Set name of service and instance response object
	global void setServiceRestBaseName(String nameService){
		this.responseData = new ReturnRestTO(nameService);
	}

	//Assign data to be returned
	global void returnSuccess(Integer statusCodeHttp, List<Object> data){
		this.responseData.meta.statusHttp = statusCodeHttp;
		this.responseData.meta.message = 'OK';
		this.responseData.data = data;
		this.executeReturn(this.responseData);
	}

	//Assign data and erros to be returned
	global void returnSuccess(Integer statusCodeHttp, List<Object> data, List<Object> errors){
		this.responseData.meta.statusHttp = statusCodeHttp;
		this.responseData.meta.message = 'OK';
		this.responseData.data = data;
		this.responseData.errors = errors;
		this.executeReturn(this.responseData);
	}

	//Assign data error to be return
	global void returnError(String errorMessage, Integer statusCode){
		this.responseData.meta.statusHttp = statusCode;
		this.responseData.meta.message = errorMessage;
		this.executeReturn(this.responseData);
	}

	//Assign data error to be returned
	global void returnError(String errorMessage){
		this.responseData.meta.statusHttp = 500;
		this.responseData.meta.message = errorMessage;
		this.executeReturn(this.responseData);
	}

	//Generate response to who called the service
	private void executeReturn(ReturnRestTO returnRestTO){

		if (this.isReturnMetadata){
			this.response.responseBody = Blob.valueOf(JSON.serialize(JSON.deserializeUntyped(JSON.serialize(returnRestTO))));
		}
		this.response.statusCode = returnRestTO.meta.statusHttp;
		this.response.addHeader('Content-Type', 'application/json');
	}
}