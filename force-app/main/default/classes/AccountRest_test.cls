/*******************************************************************************
 *                               Werise - 2020
 *-------------------------------------------------------------------------------
 *
 *    Class to test the AccountRest process
 *
 * NAME: AccountRest_test
 *
 * AUTHOR: João Vitor Ramos                                DATE: 12/04/2021
 *******************************************************************************/
@isTest
public class AccountRest_test{
	@isTest
	public static void testSuccessB2C(){
		String payload = '{"contas":[{"Name": "Conta Empresarial","codpessoa__c":"12345","RecordType":{"Name":"Cliente Pessoa Jurídica"}}]}';

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/Conta';
		request.httpMethod = 'PUT';
		request.requestBody = Blob.valueOf(payload);
		RestContext.request = request;
		RestContext.response = new RestResponse();

		Test.startTest();
		AccountRest.processPut();
		Test.stopTest();

		//get response in map
		Map<String, Object> mapReturnObjectsByParam = (Map<String, Object>)JSON.deserializeUntyped(RestContext.response.responseBody.toString());
		ReturnRestTO.MetaTO metaTO = (ReturnRestTO.MetaTO)JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('meta')), ReturnRestTO.MetaTO.class);

		//verify if the response is positive
		System.assertEquals(200, RestContext.response.statusCode);
		System.assertEquals(200,metaTO.statusHttp);

		List<Account> accounts = [SELECT Id, Name, codpessoa__c FROM Account];

		//verify the created lead
		System.assertEquals(1, accounts.size());
	}

	@isTest
	public static void testSuccessB2B(){

		String payload = '{"contas":[{"LastName": "Conta Pessoal","codpessoa__c":"54321","RecordType":{"Name":"Cliente Pessoa Física"}}]}';

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/Conta';
		request.httpMethod = 'PUT';
		request.requestBody = Blob.valueOf(payload);
		RestContext.request = request;
		RestContext.response = new RestResponse();

		Test.startTest();
		AccountRest.processPut();
		Test.stopTest();

		//get response in map
		Map<String, Object> mapReturnObjectsByParam = (Map<String, Object>)JSON.deserializeUntyped(RestContext.response.responseBody.toString());
		ReturnRestTO.MetaTO metaTO = (ReturnRestTO.MetaTO)JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('meta')), ReturnRestTO.MetaTO.class);

		//verify if the response is positive
		System.assertEquals(200, RestContext.response.statusCode);
		System.assertEquals(200, metaTO.statusHttp);

		List<Account> accounts = [SELECT Id, Name FROM Account];

		//verify the created lead
		System.assertEquals(accounts.size(), 1);
	}

	@isTest
	public static void testError(){
		//wrong payload
		String payload = '{}';

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/Conta';
		request.httpMethod = 'PUT';
		request.requestBody = Blob.valueOf(payload);
		RestContext.request = request;
		RestContext.response = new RestResponse();

		Test.startTest();
		AccountRest.processPut();
		Test.stopTest();

		//get response in map
		Map<String, Object> mapReturnObjectsByParam = (Map<String, Object>)JSON.deserializeUntyped(RestContext.response.responseBody.toString());
		ReturnRestTO.MetaTO metaTO = (ReturnRestTO.MetaTO)JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('meta')), ReturnRestTO.MetaTO.class);

		//verify if the response is positive
		System.assertEquals(RestContext.response.statusCode, 400);
		System.assertEquals(metaTO.statusHttp, 400);

		List<Account> accounts = [SELECT Id, Name, codpessoa__c FROM Account];

		//verify the created lead
		System.assertEquals(accounts.size(), 0);
	}

	@isTest
	public static void testErrorNoData(){
		//wrong payload
		String payload = '{"accounts": []}';

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/Conta';
		request.httpMethod = 'PUT';
		request.requestBody = Blob.valueOf(payload);
		RestContext.request = request;
		RestContext.response = new RestResponse();

		Test.startTest();
		AccountRest.processPut();
		Test.stopTest();

		//get response in map
		Map<String, Object> mapReturnObjectsByParam = (Map<String, Object>)JSON.deserializeUntyped(RestContext.response.responseBody.toString());
		ReturnRestTO.MetaTO metaTO = (ReturnRestTO.MetaTO)JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('meta')), ReturnRestTO.MetaTO.class);

		//verify if the response is positive
		System.assertEquals(RestContext.response.statusCode, 400);
		System.assertEquals(metaTO.statusHttp, 400);

		List<Account> accounts = [SELECT Id, Name, codpessoa__c FROM Account];

		//verify the created lead
		System.assertEquals(accounts.size(), 0);
	}
}