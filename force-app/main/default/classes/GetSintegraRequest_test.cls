/*******************************************************************************
*                               Werise - 2021
*-------------------------------------------------------------------------------
*
*    Class to test the GetSintegraRequest process
*
* NAME: GetSintegraRequest_test
*
* AUTHOR: João Vitor Ramos                                DATE: 12/04/2021
*******************************************************************************/
@isTest
public class GetSintegraRequest_test {
    @isTest
    public static void testOk() {      
        List<Account> accList = new List<Account>();

        //Calling the HttpCalloutMock class to fake the callout and test the code
        Test.setMock(HttpCalloutMock.class, new HttpCalloutMockSintegra()); 

        //Retrieving the RecordTypeId, creating the account and inserting it as a list
        Id rt = [SELECT id FROM RecordType WHERE DeveloperName = 'Cliente_Pessoa_Juridica'].Id;
        Account acc = new Account();
        acc.Name = ('Account Test');
        acc.RecordTypeId = rt;
        ACC.Cnpj__c = '07819090000196'; //cnpj used in the tests in the dev org
        acc.inscricaorg__c = '0018011060058';
        accList.add(acc);

        insert accList;
        System.debug(accList);
    }
}