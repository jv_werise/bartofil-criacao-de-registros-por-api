/*******************************************************************************
*                               Werise - 2021
*-------------------------------------------------------------------------------
*
*    Class to test the GetReceitaRequest process
*
* NAME: GetReceitaRequest_test
*
* AUTHOR: João Vitor Ramos                                DATE: 12/04/2021
*******************************************************************************/
@isTest
public class GetReceitaRequest_test {
    @isTest
    public static void testOk() {
        List<Account> accList = new List<Account>();

        //Calling the HttpCalloutMock class to fake the callout and test the code
        Test.setMock(HttpCalloutMock.class, new HttpCalloutMockReceita()); 

        //Retrieving the RecordTypeId, creating the account and inserting it as a list
        Id rt = [SELECT id FROM RecordType WHERE DeveloperName = 'Cliente_Pessoa_Juridica'].Id;
        Account acc = new Account();
        acc.Name = ('Account Test');
        acc.RecordTypeId = rt;
        acc.Cnpj__c = '07819090000196'; //cnpj used in the tests in the dev org
        accList.add(acc);

        insert accList;
    }
}