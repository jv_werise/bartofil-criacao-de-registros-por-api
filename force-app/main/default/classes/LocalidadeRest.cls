/*******************************************************************************
 *                               Werise - 2021
 *-------------------------------------------------------------------------------
 *
 *    Class to receive the Post data of Bartofil system.
 *
 * NAME: LocalidadeRest
 * TESTCLASS: LocalidadeRest_test
 *
 * AUTHOR: João Vitor Ramos                                DATE: 10/03/2021
 *******************************************************************************/
@RestResource(urlMapping = '/Localidade')
global class LocalidadeRest extends ServiceRestBase{
	private static final LocalidadeRest instance = new LocalidadeRest();
	global LocalidadeRest(){
		super.setServiceRestBase('LocalidadeService');
	}

	@HttpPut
	global static void processPut(){
		instance.processService('PUT');
	}

	global override void processService(String method){
		try{
            list<Localidade__c> successList = new list<Localidade__c>();
            List<ReturnRestTO.Errors> returnErros = new List<ReturnRestTO.Errors>();
            Map<String, Object> locResultMap = new Map<String, Object>();

            super.setServiceRestBaseName('Criar-Atualizar-Localidades');
            Map<String, Object> locMap = (Map<String, Object>)JSON.deserializeUntyped(RestContext.request.requestBody.toString());
            List<Localidade__c> locList = (List<Localidade__c>)JSON.deserialize(JSON.serialize(locMap.get('localidades')), List<Localidade__c>.class);
            System.debug(locList);

            //validate if has data
            if (locList.size() == 0){
                this.returnError('NO RECORDS', 400);
                return;
            }

            /**upsert the Cases and validate the result*/
					Database.UpsertResult[] results = Database.upsert(locList,Localidade__c.seqlocalidade__c, false);
					Integer index = 0;
					for (Database.UpsertResult ur : results){
						Localidade__c cs = locList[index];
						if (ur.isSuccess()){
							successList.add(cs);
							index++;
						} else{
							returnErros.add(new ReturnRestTO.Errors(ur.getErrors()[0].getMessage(), cs));
							index++;
						}
					}
					this.returnSuccess(200, successList, returnErros);
		} catch (Exception e){
			if (e != null){
				this.returnError(e.getMessage()+' - ' + e.getLineNumber(), 400);
			} else{
				this.returnError(e.getMessage()+' - ' + e.getLineNumber());
			}
		}
	}
}
