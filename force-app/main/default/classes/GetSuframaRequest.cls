/*******************************************************************************
*                               Werise - 2021
*-------------------------------------------------------------------------------
*
*    GetSuframaRequest
*
* NAME: GetSuframaRequest
* TEST CLASS: GetSuframaRequest_test
*
* AUTHOR: Giuliana Orsatti                                DATE: 23/03/2021
*******************************************************************************/
public class GetSuframaRequest implements Queueable, Database.AllowsCallouts{
    private String cnpj;
    public GetSuframaRequest(String cnpjString){
        this.cnpj = cnpjString;
    }
    public void execute(QueueableContext context){
        try{
            //Make the http get request
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint('https://bartofil.hubin.io/iomanager/api/flows/execute/route/suframa/'+this.cnpj);
            request.setMethod('GET');
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Charset', 'UTF-8');
            request.setTimeout(120000);
            HttpResponse response = http.send(request);
            if (response.getStatusCode() == 200){
                System.debug('response: ' + response.getStatusCode());
                System.debug('response: ' + response.getBody());
            } else{
                System.debug('response: ' + response.getStatusCode());
                System.debug('response: ' + response.getBody());
            }
        } catch (Exception e){
            System.debug('ERROR: ' + e.getMessage()+'. In line: ' + e.getLineNumber());
        }
    }
}