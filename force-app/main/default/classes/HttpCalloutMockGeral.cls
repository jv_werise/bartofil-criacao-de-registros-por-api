/*******************************************************************************
*                               Werise - 2021
*-------------------------------------------------------------------------------
*
*    Class to create mock callout request and return the expected body
*
* USED IN: AccountTriggerHandler_test
* NAME: HttpCalloutMockGeral
*
* AUTHOR: João Vitor Ramos                                DATE: 12/04/2021
*******************************************************************************/
@isTest
global class HttpCalloutMockGeral implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"status":"OK", "message":"Callout testado com sucesso!"}');
        response.setStatusCode(200);
        return response; 
    }
}
