/*******************************************************************************
 *                               Werise - 2021
 *-------------------------------------------------------------------------------
 *
 *    Class to test the LocalidadeRest process
 *
 * NAME: LocalidadeRest_test
 *
 * AUTHOR: João Vitor Ramos                               DATE: 10/03/2021
 *******************************************************************************/
@isTest
public class LocalidadeRest_test{
	@isTest
	public static void testSuccess(){
		String payload = '{"localidades": ['+
                                        '{"Name": "teste loc 1","seqlocalidade__c": "1"},'+
                                        '{"Name": "teste loc 2","seqlocalidade__c": "2"}]}';
        String payload2 = '{"localidades": ['+
                                        '{"Name": "teste loc 3","seqlocalidade__c": "1"},'+
                                        '{"Name": "teste loc 4","seqlocalidade__c": "4"}]}';

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/localidade';
		request.httpMethod = 'PUT';
		request.requestBody = Blob.valueOf(payload);
		RestContext.request = request;
		RestContext.response = new RestResponse();

		Test.startTest();
		LocalidadeRest.processPut();
        request.requestBody = Blob.valueOf(payload2);
        LocalidadeRest.processPut();
		Test.stopTest();

		//get response in map
		Map<String, Object> mapReturnObjectsByParam = (Map<String, Object>)JSON.deserializeUntyped(RestContext.response.responseBody.toString());
		ReturnRestTO.MetaTO metaTO = (ReturnRestTO.MetaTO)JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('meta')), ReturnRestTO.MetaTO.class);

		//verify if the response is positive
		System.assertEquals(RestContext.response.statusCode, 200);
		System.assertEquals(metaTO.statusHttp, 200);

		List<Localidade__c> locList = [SELECT Id, Name, seqlocalidade__c
		                          FROM Localidade__c];

		//verify the created lead
		System.assertEquals(locList.size(), 3);
	}

	@isTest
	public static void testError(){
		//wrong payload
		String payload = '{}';

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/localidade';
		request.httpMethod = 'PUT';
		request.requestBody = Blob.valueOf(payload);
		RestContext.request = request;
		RestContext.response = new RestResponse();

		Test.startTest();
		LocalidadeRest.processPut();
		Test.stopTest();

		//get response in map
		Map<String, Object> mapReturnObjectsByParam = (Map<String, Object>)JSON.deserializeUntyped(RestContext.response.responseBody.toString());
		ReturnRestTO.MetaTO metaTO = (ReturnRestTO.MetaTO)JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('meta')), ReturnRestTO.MetaTO.class);

		//verify if the response is positive
		System.assertEquals(RestContext.response.statusCode, 400);
		System.assertEquals(metaTO.statusHttp, 400);

		List<Localidade__c> loclist = [SELECT Id, Name, seqlocalidade__c
		                          FROM Localidade__c];

		//verify the created lead
		System.assertEquals(locList.size(), 0);
	}

	@isTest
	public static void testErrorNoData(){
		//wrong payload
		String payload = '{"localidades": []}';

		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/localidade';
		request.httpMethod = 'PUT';
		request.requestBody = Blob.valueOf(payload);
		RestContext.request = request;
		RestContext.response = new RestResponse();

		Test.startTest();
		LocalidadeRest.processPut();
		Test.stopTest();

		//get response in map
		Map<String, Object> mapReturnObjectsByParam = (Map<String, Object>)JSON.deserializeUntyped(RestContext.response.responseBody.toString());
		ReturnRestTO.MetaTO metaTO = (ReturnRestTO.MetaTO)JSON.deserialize(JSON.serialize(mapReturnObjectsByParam.get('meta')), ReturnRestTO.MetaTO.class);

		//verify if the response is negative
		System.assertEquals(RestContext.response.statusCode, 400);
		System.assertEquals(metaTO.statusHttp, 400);

		List<Localidade__c> locList = [SELECT Id, Name, seqlocalidade__c
		                          FROM Localidade__c];

		//verify the created lead
		System.assertEquals(locList.size(), 0);
	}
}
