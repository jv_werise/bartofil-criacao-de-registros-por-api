/*******************************************************************************
 *                               Werise - 2021
 *-------------------------------------------------------------------------------
 *
 *    Trigger on Account to handle logic on dml events of Bartofil system.
 *
 * NAME: AccountTrigger
 * TESTCLASS: AccountTriggerHandler_test
 *
 * AUTHOR: João Vitor Ramos                                DATE: 23/03/2021
 *******************************************************************************/
trigger AccountTrigger on Account (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    if(Trigger.isInsert){
        if(Trigger.isAfter){
            List<Account> AccUIList = new List<Account>();
            List<Account> cpjList = new List<Account>();
            List<Account> cpfList = new List<Account>();

            //Logic to fulfill the Conta_em_Rede__c field with parant account
            AccountTriggerHandler.atribuirContaEmRedeCpf(Trigger.new);
            AccountTriggerHandler.atribuirContaEmRedeCnpj(Trigger.new);

            //Call queueable class that retrieve government information and send the accounts to Bartofil
            if(Test.isRunningTest()){
                System.enqueueJob(new GetReceitaRequest(Trigger.new));
                System.enqueueJob(new GetSintegraRequest(Trigger.new));
            }else{
                System.enqueueJob(new GetReceitaRequest(Trigger.new));
            }

            //Filtering the accounts who was created by UI
            for(Account acc : Trigger.new){
                if(acc.Criado_Bartofil__c == false){
                    AccUIList.add(acc);
                }
            }

            //Sending the accounts to Bartofil's System
            if(!AccUIList.isEmpty()){
                AccountTriggerHandler.enviarContasBartofil(AccUIList);
            }
        }
    }
}